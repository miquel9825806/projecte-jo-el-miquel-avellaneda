package Exercici_Joel;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Exercici_Joel_JUnit {
	@Test
	public void testPrograma1() {
		int casos = 6;
        String[] numeros = {"1", "2", "3", "4", "5", "6"};
        int sumaEsperada = 21;
        int restaEsperada = -19;
        int multiplicacionEsperada = 720;
        float divisionEsperada = (float) 0.0013889;
        int cas = 2;

        int ResultatResta = Integer.parseInt(numeros[0]);
        for(int i = 1; i < numeros.length; i++) {
        	ResultatResta =  ResultatResta - Integer.parseInt(numeros[i]);
        }
        assertEquals(restaEsperada, ResultatResta);
    }
	@Test
	public void testPrograma2() {
		int casos = 3;
        String[] numeros = {"6", "10", "3"};
        int sumaEsperada = 19;
        int restaEsperada = -7;
        int multiplicacioEsperada = 180;
        float divisioEsperada = (float) 0.2;
        int cas = 3;

        int Resultatmultiplicacio = Integer.parseInt(numeros[0]);
        for(int i = 1; i < numeros.length; i++) {
        	Resultatmultiplicacio =  Resultatmultiplicacio * Integer.parseInt(numeros[i]);
        }
        assertEquals(multiplicacioEsperada, Resultatmultiplicacio);
    }
	@Test
	public void testPrograma3() {
		int casos = 4;
        String[] numeros = {"9", "1", "4", "6"};
        int sumaEsperada = 20;
        int restaEsperada = -2;
        int multiplicacioEsperada = 216;
        float divisioEsperada = (float) 0.375;
        int cas = 1;

        int ResultatSuma = Integer.parseInt(numeros[0]);
        for(int i = 1; i < numeros.length; i++) {
        	ResultatSuma =  ResultatSuma + Integer.parseInt(numeros[i]);
        }
        assertEquals(sumaEsperada, ResultatSuma);
    }
	@Test
	public void testPrograma4() {
		int casos = 7;
        String[] numeros = {"7", "5", "2", "4", "3", "7", "5"};
        int sumaEsperada = 33;
        int restaEsperada = -19;
        int multiplicacionEsperada = 29400;
        float divisionEsperada = (float) 0.00166667;
        int cas = 4;

        int ResultatDivisio = Integer.parseInt(numeros[0]);
        for(int i = 1; i < numeros.length; i++) {
        	ResultatDivisio =  ResultatDivisio / Integer.parseInt(numeros[i]);
        }
        assertEquals(restaEsperada, ResultatDivisio);
    }
	@Test
	public void testPrograma5() {
		int casos = 3;
        String[] numeros = {"5", "8", "7"};
        int sumaEsperada = 20;
        int restaEsperada = -10;
        int multiplicacionEsperada = 280;
        float divisionEsperada = (float) 0.0892857143;
        int cas = 3;

        // Llamada al método programa para obtener los resultados
        int Resultatmultiplicacio = Integer.parseInt(numeros[0]);
        for(int i = 1; i < numeros.length; i++) {
        	Resultatmultiplicacio =  Resultatmultiplicacio * Integer.parseInt(numeros[i]);
        }
        assertEquals(restaEsperada, Resultatmultiplicacio);
    }
}