package Exercici_Joel;

import java.util.ArrayList;
import java.util.Scanner;
public class Exercici_Joel {
	public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
        int numtot = sc.nextInt();
        String llista_numeros[] = new String[numtot];
        for(int i = 0; i < numtot; i++) {
        	/**
             * Llegeix un número ingressat per l'usuari i l'afegeix a la llista de números.
              * Si el número ingressat és zero, sol·licita a l'usuari que ingressi un altre número.
              * @param llista_numeros Llista de números ingressats per l'usuari.
              * @param sc Objecte Scanner per llegir l'entrada de l'usuari.
             */
        	llista_numeros[i] = sc.next();
        }
    	programa(llista_numeros, numtot, sc);
    }
    /**
     * Realitza els càlculs de suma, resta, multiplicació i divisió sobre la llista de números.
      * @param llista_numeros Llista de números ingressats per l'usuari.
     * @param sc 
      * @param casos Nombre total de casos a processar.
     */
    public static void programa(String[] llista_numeros, int numtot, Scanner sc) {
    	int suma = Integer.parseInt(llista_numeros[0]);
        int resta = Integer.parseInt(llista_numeros[0]);
        int multiplicacio = Integer.parseInt(llista_numeros[0]);
        float divisio = Float.parseFloat(llista_numeros[0]);
        for(int i = 1; i < numtot; i++) {
            suma = suma + Integer.parseInt(llista_numeros[i]);
            resta = resta - Integer.parseInt(llista_numeros[i]);
            multiplicacio = multiplicacio * Integer.parseInt(llista_numeros[i]);
            divisio = divisio / Float.parseFloat(llista_numeros[i]);
        }
        resultats(suma, resta, multiplicacio, divisio, sc);
    }
    /**
     * Mostra els resultats de les operacions aritmètiques.
      * @param suma Resultat de la suma.
      * @param resta Resultat de la resta.
      * @param multiplicacio Resultat de la multiplicació.
      * @param divisio Resultat de la divisió.
     * @param sc 
     */
    public static void resultats(int suma, int resta, int multiplicacio, float divisio, Scanner sc) {
    	System.out.println("Escull una opció de calcul:");
    	System.out.println("1. Suma");
    	System.out.println("2. Resta");
    	System.out.println("3. Multiplicacio");
    	System.out.println("4. Divisio");
    	int cas = sc.nextInt();
    	switch (cas) {
    	case 1:
    		System.out.println(suma);
    		break;
    	case 2:
    		System.out.println(resta);
    		break;
    	case 3:
    		System.out.println(multiplicacio);
    		break;
    	case 4:
    		System.out.println(divisio);
    		break;
    	
    }

}
}